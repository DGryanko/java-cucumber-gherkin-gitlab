import org.junit.Before;
import org.openqa.selenium.WebDriver;
import webdriver.ChromeWebDriverProvider;

public abstract class BaseTestConfig {

    protected WebDriver webDriver;
    protected ChromeWebDriverProvider chromeWebDriverProvider;

    @Before
    public void beforeTest(){
    }

}
