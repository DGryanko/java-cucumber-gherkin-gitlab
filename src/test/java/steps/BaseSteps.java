package steps;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.cucumber.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utilities.Guid;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.withText;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class BaseSteps {

    protected static final Dimension DEFAULT_BROWSER_SIZE = new Dimension(1680, 1050);

    @Given("Открыть URL \"([^\"]*)\"$")
    public void openToUrlOnDesktopWithDefaultSize(String url) {
        openToUrlWithCustomSize(url, DEFAULT_BROWSER_SIZE);
    }

    @Given("Подождать секунд \"([^\"]*)\"$")
    public void waitTime(int seconds) {
        try {
            Thread.sleep(seconds * 1000L);
            System.out.println("Подождать секунд" + seconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Given("В блоке \"([^\"]*)\" нажать на ссылку \"([^\"]*)\"$")
    public void clickLinkInBlock(String name, String button) {
        $(By.xpath("//*[@class='" + name + "']//*[contains(@href, '" + button + "')]")).hover().click();
        System.out.println("В блоке " + name + " нажать ссылку " + button);
    }

    @Given("Открыта страница с URL \"([^\"]*)\"$")
    public void checkUrl(String uuid) {
        uuid = Guid.generateUUID(uuid);
        Wait().until(ExpectedConditions.urlContains(uuid));
        System.out.println("Открыта страница с URL " + uuid);
    }

    @Given("На странице отображается текст \"([^\"]*)\"$")
    public void textPageEnabled(String uuid) {
        uuid = Guid.generateUUID(uuid);
        $$(withText(uuid)).findBy(visible).shouldBe(visible);
        System.out.println("На странице отображается текст " + uuid);
    }

    @Given("На странице не отображается текст \"([^\"]*)\"$")
    public void textPageNotVisible(String text) {
        String xPath = "//*[contains(text(),'" + text + "')]";
        $(By.xpath(xPath)).shouldNotBe(visible);
        System.out.println("На странице не отображается текст " + text);
    }

    @Given("Поле \"([^\"]*)\" заполнить значением \"(.*)\"$")
    public void setFieldValue(String field, String uuid) {
        uuid = Guid.generateUUID(uuid);
        String xPath = "//*[@class='" + field + "']";
        sleep(300);
        $(By.xpath(xPath)).shouldNotBe(disabled).sendKeys(Keys.CONTROL, "a", Keys.DELETE);
        $(By.xpath(xPath)).shouldNotBe(disabled).setValue(uuid);
        System.out.println("Поле " + field + " заполнить значением " + uuid);
    }

    @Given("Поле \"([^\"]*)\" заполнить значением \"([^\"]*)\" и нажать Enter$")
    public void setValueToFieldAndEnter(String field, String uuid) {
        uuid = Guid.generateUUID(uuid);
        String xPath = "//*[@class='" + field + "']";
        SelenideElement element = $(By.xpath(xPath));
        element.sendKeys(Keys.CONTROL, "a", Keys.DELETE);
        Condition sendKeysPrecondition = or("sendKeysPrecondition", Condition.value("+380"), empty);
        element.shouldBe(sendKeysPrecondition);
        element.sendKeys(uuid);
        sleep(1000);
        element.shouldBe(not(empty)).pressEnter();
        System.out.println("Поле " + field + " заполнить значением " + uuid + " и нажать Enter");
    }

    @Given("На странице нажать кнопку \"([^\"]*)\"$")
    public void clickButton(String button) {
        SelenideElement element = $x("//*[@class='" + button + "']");
        element.shouldBe(exist);
        element.shouldNotBe(disabled);
        element.scrollIntoView("{block: \"center\"}");
        Selenide.executeJavaScript("window.scrollBy(0,20)");
        sleep(500);
        element.hover();
        element.click();
        System.out.println("На странице нажать кнопку " + button);
    }

    @Given("В блоке \"([^\"]*)\" нажать кнопку \"([^\"]*)\"$")
    public void clickButtonInBlock(String name, String button) {
        SelenideElement element = $x("//*[@class='" + name + "']//*[@data='" + button + "']");
        element.shouldBe(exist);
        element.shouldNotBe(disabled);
        element.scrollIntoView("{block: \"center\"}");
        Selenide.executeJavaScript("window.scrollBy(0,20)");
        sleep(500);
        element.hover();
        element.click();
        System.out.println("В блоке " + name + " нажать кнопку " + button);
    }

    public void openToUrlWithCustomSize(String url, Dimension browserSize) {
        String test = System.getenv("URL");
        if (test == null) {
            open(url);
        } else {
            open(test);
        }

        getWebDriver().manage().window().setSize(browserSize);
        System.out.println("Открыть URL " + url);
    }

}
