# new feature
# Tags: optional

#ТК 0 - Проверка кнопки логина

Feature: Auth
  @TC-1
  Scenario Outline: TC-1 - Проверка кнопки логина
    Given Открыть URL "<web>"
    And Открыта страница с URL "<link0>"
    And На странице отображается текст "Войти"
    And В блоке "<block>" нажать на ссылку "<link1>"
    And Открыта страница с URL "<link3>"
    And Поле "whsOnd zHQkBf" заполнить значением "<email_mok>" и нажать Enter
    And На странице отображается текст "Не удалось войти в аккаунт"
    And Подождать секунд "1"

    Examples:
      | web               | block   | link0     | link1               | link3   | email_mok             |
      | https://google.com| gb_We   | google.com| accounts.google.com/| /signin/| gryanko.life@gmail.com|