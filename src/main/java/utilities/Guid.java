package utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

public class Guid {

    public static final ThreadLocal<List<String>> variableList = ThreadLocal.withInitial(ArrayList::new);
    private static final ThreadLocal<List<String>> uuidList = ThreadLocal.withInitial(ArrayList::new);
    private static final List<String> timestampList = new ArrayList<>();
    private static final List<String> randomList = new ArrayList<>();


    static {
        uuidListInitialize();
        variableListInitialize();
        randomListInitialize();
        try {
            timestampListInitialize();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String generateUUID(String uuid) {

        if (uuid.startsWith("GUID")) {
            int uuidIndex = Integer.parseInt(uuid.substring(uuid.indexOf('[') + 1, uuid.length() - 1));
            uuid = uuidList.get().get(uuidIndex);
        } else if (uuid.contains("_GUID")) {
            String[] subStrings = uuid.split("_");
            int uuidIndex = Integer.parseInt(subStrings[1].substring(subStrings[1].indexOf('[') + 1, subStrings[1].length() - 1));
            subStrings[1] = uuidList.get().get(uuidIndex);
            uuid = subStrings[0] + "_" + subStrings[1] + "_" + subStrings[2];
        } else if (uuid.contains("RANDOM_PHONE")) {
            uuid = String.valueOf(System.currentTimeMillis());
            uuid = uuid.substring(1, 10);
        } else if (uuid.startsWith("TIMESTAMP")) {
            int uuidIndex = Integer.parseInt(uuid.substring(uuid.indexOf('[') + 1, uuid.length() - 1));
            uuid = timestampList.get(uuidIndex);
        } else if (uuid.contains("_TIMESTAMP")) {
            String[] subStrings = uuid.split("_");
            int uuidIndex = Integer.parseInt(subStrings[1].substring(subStrings[1].indexOf('[') + 1, subStrings[1].length() - 1));
            subStrings[1] = timestampList.get(uuidIndex);
            uuid = subStrings[0] + "_" + subStrings[1] + "_" + subStrings[2];
        } else if (uuid.startsWith("RANDOM")) {
            int uuidIndex = Integer.parseInt(uuid.substring(uuid.indexOf('[') + 1, uuid.length() - 1));
            uuid = randomList.get(uuidIndex);
        } else if (uuid.startsWith("VARIABLE")) {
            Pattern pattern = Pattern.compile("(?<=\\[).+?(?=\\])");
            Matcher matcher = pattern.matcher(uuid);
            if (matcher.find()) {
                int idx = Integer.parseInt(matcher.group());
                uuid = variableList.get().get(idx);
            } else {
                uuid = variableList.get().get(0);
            }
        } else if (uuid.contains("VARIABLE") & !uuid.startsWith("VARIABLE")) {
            uuid = uuid.replace("VARIABLE", variableList.get().get(0));
        }

        return uuid;
    }

    public static void uuidListInitialize() {
        uuidList.get().clear();
        for (int i = 0; i < 10; i++) {
            uuidList.get().add(UUID.randomUUID().toString());
        }
    }

    public static void variableListInitialize() {
        variableList.get().clear();
    }

    private static void timestampListInitialize() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            timestampList.add(String.valueOf(System.currentTimeMillis()));
            sleep(1);
        }
    }

    private static void randomListInitialize() {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            randomList.add(String.valueOf(random.nextInt(18) + 10));
        }
    }

}