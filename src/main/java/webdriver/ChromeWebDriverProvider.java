package webdriver;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverProvider;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public class ChromeWebDriverProvider implements WebDriverProvider {

    public static ChromeOptions getChromeOptions() {
        ChromeOptions chromeOptions = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("profile.default_content_setting_values.notifications", 2);
        prefs.put("credentials_enable_service", false);
        prefs.put("profile.password_manager_enabled", false);
        chromeOptions.addArguments("--disable-infobars");
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--disable-notifications");

        if (Configuration.headless) {
            chromeOptions.addArguments("--headless");
        }

        Object geolocationCap = Configuration.browserCapabilities.getCapability("profile.default_content_setting_values.geolocation");
        int geolocation = geolocationCap != null ? Integer.parseInt(geolocationCap.toString()) : 0;
        prefs.put("profile.default_content_setting_values.geolocation", geolocation);

        chromeOptions.setExperimentalOption("prefs", prefs);
        return chromeOptions;
    }

    @Nonnull
    @Override
    public WebDriver createDriver(@Nonnull Capabilities capabilities) {
        WebDriverManager.chromedriver().setup();

        return new ChromeDriver(getChromeOptions());
    }

}
